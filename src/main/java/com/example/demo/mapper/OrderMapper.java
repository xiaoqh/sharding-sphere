package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.controller.OrderInfo;
import com.example.demo.controller.OrderInfo1;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by xqh on 2021/3/27 17:17
 *
 * @Description
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    List<OrderInfo> findOrderInfos(@Param("type") String type);

    List<OrderInfo1> statistic1(@Param("type") String type);

    List<OrderItem> pageList(@Param("type") String type, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);
}
