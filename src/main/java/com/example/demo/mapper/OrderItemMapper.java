package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by xqh on 2021/3/27 17:17
 *
 * @Description
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {
}
