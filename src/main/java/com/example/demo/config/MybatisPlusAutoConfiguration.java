//package com.example.demo.config;
//import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
//import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
//import com.baomidou.mybatisplus.autoconfigure.MybatisPlusPropertiesCustomizer;
//import com.baomidou.mybatisplus.autoconfigure.SpringBootVFS;
//import com.baomidou.mybatisplus.core.MybatisConfiguration;
//import com.baomidou.mybatisplus.core.config.GlobalConfig;
//import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
//import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
//import com.baomidou.mybatisplus.core.injector.ISqlInjector;
//import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
//import io.shardingsphere.shardingjdbc.jdbc.core.datasource.ShardingDataSource;
//import io.shardingsphere.shardingjdbc.spring.boot.SpringBootConfiguration;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.mapping.DatabaseIdProvider;
//import org.apache.ibatis.plugin.Interceptor;
//import org.apache.ibatis.session.ExecutorType;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.SqlSessionFactoryBean;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.mybatis.spring.mapper.ClassPathMapperScanner;
//import org.mybatis.spring.mapper.MapperFactoryBean;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.BeanFactory;
//import org.springframework.beans.factory.BeanFactoryAware;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.ObjectProvider;
//import org.springframework.beans.factory.support.BeanDefinitionRegistry;
//import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
//import org.springframework.boot.autoconfigure.AutoConfigureAfter;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ResourceLoaderAware;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
//import org.springframework.core.io.Resource;
//import org.springframework.core.io.ResourceLoader;
//import org.springframework.core.type.AnnotationMetadata;
//import org.springframework.util.Assert;
//import org.springframework.util.CollectionUtils;
//import org.springframework.util.ObjectUtils;
//import org.springframework.util.StringUtils;
//
//import javax.sql.DataSource;
//import java.util.List;
//
//
///**
// * Created by xqh on 2021/3/27 17:43
// *
// * @Description
// */
//
//@Slf4j
//@Configuration
//@ConditionalOnClass({SqlSessionFactory.class, SqlSessionFactoryBean.class})
////@ConditionalOnSingleCandidate(ShardingDataSource.class)
//@EnableConfigurationProperties(MybatisPlusProperties.class)
//@AutoConfigureAfter(SpringBootConfiguration.class)
//public class MybatisPlusAutoConfiguration  implements InitializingBean {
//
//
//    private final MybatisPlusProperties properties;
//
//    private final Interceptor[] interceptors;
//
//    private final ResourceLoader resourceLoader;
//
//    private final DatabaseIdProvider databaseIdProvider;
//
//    private final List<ConfigurationCustomizer> configurationCustomizers;
//
//    private final List<MybatisPlusPropertiesCustomizer> mybatisPlusPropertiesCustomizers;
//
//    private final ApplicationContext applicationContext;
//
//
//    public MybatisPlusAutoConfiguration(MybatisPlusProperties properties,
//                                        ObjectProvider<Interceptor[]> interceptorsProvider,
//                                        ResourceLoader resourceLoader,
//                                        ObjectProvider<DatabaseIdProvider> databaseIdProvider,
//                                        ObjectProvider<List<ConfigurationCustomizer>> configurationCustomizersProvider,
//                                        ObjectProvider<List<MybatisPlusPropertiesCustomizer>> mybatisPlusPropertiesCustomizerProvider,
//                                        ApplicationContext applicationContext) {
//        this.properties = properties;
//        this.interceptors = interceptorsProvider.getIfAvailable();
//        this.resourceLoader = resourceLoader;
//        this.databaseIdProvider = databaseIdProvider.getIfAvailable();
//        this.configurationCustomizers = configurationCustomizersProvider.getIfAvailable();
//        this.mybatisPlusPropertiesCustomizers = mybatisPlusPropertiesCustomizerProvider.getIfAvailable();
//        this.applicationContext = applicationContext;
//    }
//
//    @Override
//    public void afterPropertiesSet() {
//        if (!CollectionUtils.isEmpty(mybatisPlusPropertiesCustomizers)) {
//            mybatisPlusPropertiesCustomizers.forEach(i -> i.customize(properties));
//        }
//        checkConfigFileExists();
//    }
//
//    private void checkConfigFileExists() {
//        if (this.properties.isCheckConfigLocation() && StringUtils.hasText(this.properties.getConfigLocation())) {
//            Resource resource = this.resourceLoader.getResource(this.properties.getConfigLocation());
//            Assert.state(resource.exists(), "Cannot find config location: " + resource
//                    + " (please add config file or check your Mybatis configuration)");
//        }
//    }
//
//    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
//    @Bean
//    @ConditionalOnMissingBean
//    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
//        // TODO 使用 MybatisSqlSessionFactoryBean 而不是 SqlSessionFactoryBean
//        MybatisSqlSessionFactoryBean factory = new MybatisSqlSessionFactoryBean();
//        factory.setDataSource(dataSource);
//        factory.setVfs(SpringBootVFS.class);
//        if (StringUtils.hasText(this.properties.getConfigLocation())) {
//            factory.setConfigLocation(this.resourceLoader.getResource(this.properties.getConfigLocation()));
//        }
//        applyConfiguration(factory);
//        if (this.properties.getConfigurationProperties() != null) {
//            factory.setConfigurationProperties(this.properties.getConfigurationProperties());
//        }
//        if (!ObjectUtils.isEmpty(this.interceptors)) {
//            factory.setPlugins(this.interceptors);
//        }
//        if (this.databaseIdProvider != null) {
//            factory.setDatabaseIdProvider(this.databaseIdProvider);
//        }
//        if (StringUtils.hasLength(this.properties.getTypeAliasesPackage())) {
//            factory.setTypeAliasesPackage(this.properties.getTypeAliasesPackage());
//        }
//        if (this.properties.getTypeAliasesSuperType() != null) {
//            factory.setTypeAliasesSuperType(this.properties.getTypeAliasesSuperType());
//        }
//        if (StringUtils.hasLength(this.properties.getTypeHandlersPackage())) {
//            factory.setTypeHandlersPackage(this.properties.getTypeHandlersPackage());
//        }
//        if (!ObjectUtils.isEmpty(this.properties.resolveMapperLocations())) {
//            factory.setMapperLocations(this.properties.resolveMapperLocations());
//        }
//
//        // TODO 自定义枚举包
//        if (StringUtils.hasLength(this.properties.getTypeEnumsPackage())) {
//            factory.setTypeEnumsPackage(this.properties.getTypeEnumsPackage());
//        }
//        // TODO 此处必为非 NULL
//        GlobalConfig globalConfig = this.properties.getGlobalConfig();
//        // TODO 注入填充器
//        if (this.applicationContext.getBeanNamesForType(MetaObjectHandler.class,
//                false, false).length > 0) {
//            MetaObjectHandler metaObjectHandler = this.applicationContext.getBean(MetaObjectHandler.class);
//            globalConfig.setMetaObjectHandler(metaObjectHandler);
//        }
//        // TODO 注入主键生成器
//        if (this.applicationContext.getBeanNamesForType(IKeyGenerator.class, false,
//                false).length > 0) {
//            IKeyGenerator keyGenerator = this.applicationContext.getBean(IKeyGenerator.class);
//            globalConfig.getDbConfig().setKeyGenerator(keyGenerator);
//        }
//        // TODO 注入sql注入器
//        if (this.applicationContext.getBeanNamesForType(ISqlInjector.class, false,
//                false).length > 0) {
//            ISqlInjector iSqlInjector = this.applicationContext.getBean(ISqlInjector.class);
//            globalConfig.setSqlInjector(iSqlInjector);
//        }
//        // TODO 设置 GlobalConfig 到 MybatisSqlSessionFactoryBean
//        factory.setGlobalConfig(globalConfig);
//        return factory.getObject();
//    }
//
//    // TODO 入参使用 MybatisSqlSessionFactoryBean
//    private void applyConfiguration(MybatisSqlSessionFactoryBean factory) {
//        // TODO 使用 MybatisConfiguration
//        MybatisConfiguration configuration = this.properties.getConfiguration();
//        if (configuration == null && !StringUtils.hasText(this.properties.getConfigLocation())) {
//            configuration = new MybatisConfiguration();
//        }
//        if (configuration != null && !CollectionUtils.isEmpty(this.configurationCustomizers)) {
//            for (ConfigurationCustomizer customizer : this.configurationCustomizers) {
//                customizer.customize(configuration);
//            }
//        }
//        factory.setConfiguration(configuration);
//    }
//
//    @Bean
//    @ConditionalOnMissingBean
//    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
//        ExecutorType executorType = this.properties.getExecutorType();
//        if (executorType != null) {
//            return new SqlSessionTemplate(sqlSessionFactory, executorType);
//        } else {
//            return new SqlSessionTemplate(sqlSessionFactory);
//        }
//    }
//
//    /**
//     * This will just scan the same base package as Spring Boot does. If you want
//     * more power, you can explicitly use
//     * {@link org.mybatis.spring.annotation.MapperScan} but this will get typed
//     * mappers working correctly, out-of-the-box, similar to using Spring Data JPA
//     * repositories.
//     */
//    public static class AutoConfiguredMapperScannerRegistrar
//            implements BeanFactoryAware, ImportBeanDefinitionRegistrar, ResourceLoaderAware {
//
//        private BeanFactory beanFactory;
//
//        private ResourceLoader resourceLoader;
//
//        @Override
//        public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
//
//            if (!AutoConfigurationPackages.has(this.beanFactory)) {
//                return;
//            }
//
//            log.debug("Searching for mappers annotated with @Mapper");
//
//            List<String> packages = AutoConfigurationPackages.get(this.beanFactory);
//            if (log.isDebugEnabled()) {
//                packages.forEach(pkg -> log.debug("Using auto-configuration base package '{}'", pkg));
//            }
//
//            ClassPathMapperScanner scanner = new ClassPathMapperScanner(registry);
//            if (this.resourceLoader != null) {
//                scanner.setResourceLoader(this.resourceLoader);
//            }
//            scanner.setAnnotationClass(Mapper.class);
//            scanner.registerFilters();
//            scanner.doScan(StringUtils.toStringArray(packages));
//
//        }
//
//        @Override
//        public void setBeanFactory(BeanFactory beanFactory) {
//            this.beanFactory = beanFactory;
//        }
//
//        @Override
//        public void setResourceLoader(ResourceLoader resourceLoader) {
//            this.resourceLoader = resourceLoader;
//        }
//    }
//
//    /**
//     * {@link org.mybatis.spring.annotation.MapperScan} ultimately ends up
//     * creating instances of {@link MapperFactoryBean}. If
//     * {@link org.mybatis.spring.annotation.MapperScan} is used then this
//     * auto-configuration is not needed. If it is _not_ used, however, then this
//     * will bring in a bean registrar and automatically register components based
//     * on the same component-scanning path as Spring Boot itself.
//     */
//    @Configuration
//    @Import({com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration.AutoConfiguredMapperScannerRegistrar.class})
//    @ConditionalOnMissingBean(MapperFactoryBean.class)
//    public static class MapperScannerRegistrarNotFoundConfiguration implements InitializingBean {
//
//        @Override
//        public void afterPropertiesSet() {
//            log.debug("No {} found.", MapperFactoryBean.class.getName());
//        }
//    }
//}
