package com.example.demo.config;



import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;

/**
 * Created by xqh on 2021/3/27 17:24
 *
 * @Description
 */
public class DbShardingAlgorithm implements PreciseShardingAlgorithm<String> {

    private static final String DB_NAME_PREFIX = "t";
    @Override
    public String doSharding(final Collection<String> availableTargetNames, final PreciseShardingValue<String> shardingValue) {
        String targetTable = DB_NAME_PREFIX + shardingValue.getValue();
        if (availableTargetNames.contains(targetTable)){
            return targetTable;
        }
        throw new UnsupportedOperationException("无法判定的值: " + shardingValue.getValue());
    }
}
