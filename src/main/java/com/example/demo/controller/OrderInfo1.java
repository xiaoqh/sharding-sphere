package com.example.demo.controller;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by xqh on 2021/3/30 13:52
 *
 * @Description
 */
@Data
public class OrderInfo1 {

    private String orderId;
    private String orderName;
    private BigDecimal orderAmount;
    private String orderType;

    private String orderItemNames;
    private String orderItemTypes;
    private BigDecimal orderItemAmount;
    private Integer orderItemCount;
}
