package com.example.demo.controller;

import com.example.demo.model.Order;
import com.example.demo.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xqh on 2021/3/27 17:22
 *
 * @Description
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @GetMapping("/list")
    public Object list() {

        List<Order> orders = orderService.list();
        return orders;
    }

    @GetMapping("/save/{type}")
    public Object save(@PathVariable String type) {
        Order o = new Order();
        o.setName("aaaaaa");
//        o.setCreateTime(new Date());
        o.setType(type);
        return orderService.save(o);
    }

    @GetMapping("/delete/{id}/{type}")
    public Object delete(@PathVariable String id, @PathVariable String type) {
        Order o = new Order();
        o.setId(id);
        o.setType(type);
        orderService.removeById(id);
        return orderService.removeById(id);
    }

    @GetMapping("/find/{type}")
    public Object delete(@PathVariable String type) {
        return orderService.lambdaQuery().eq(Order::getType, type).list();
    }

}
