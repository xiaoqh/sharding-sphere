package com.example.demo.controller;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by xqh on 2021/3/30 13:52
 *
 * @Description
 */
@Data
public class OrderInfo {

    private String orderId;
    private String orderName;
    private String orderType;
    private BigDecimal orderAmount;
    private String orderItemName;
    private String orderItemType;
    private BigDecimal orderItemAmount;
}
