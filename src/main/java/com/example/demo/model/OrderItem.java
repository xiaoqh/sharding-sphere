package com.example.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by xqh on 2021/3/27 17:15
 *
 * @Description
 */
@Data
@TableName(value = "`order_item`")
public class OrderItem extends Model<OrderItem> {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;
    private String name;
    private String orderId;
    private String type;
    private BigDecimal amount;
}
