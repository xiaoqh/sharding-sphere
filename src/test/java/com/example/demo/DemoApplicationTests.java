package com.example.demo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.controller.OrderInfo;
import com.example.demo.controller.OrderInfo1;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.service.IOrderItemService;
import com.example.demo.service.IOrderService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sound.midi.Soundbank;
import java.math.BigDecimal;
import java.util.*;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    private IOrderService orderService;

    @Autowired
    private IOrderItemService orderItemService;

    @Autowired
    private OrderMapper orderMapper;

    @Test
    void contextLoads() {
        List<Order> orders = new ArrayList<>(5);
        List<OrderItem> orderItems = new ArrayList<>(15);
        for (int i=0; i< 10; i++){
            String type = 1 + new Random().nextInt(3) + "";
            int amount = new Random().nextInt(1000) + 300;
            Order o = new Order();
            o.setName("type-"+ type);
            o.setCreateTime(new Date());
            o.setType(type);
            o.setAmount(new BigDecimal(amount));
            orders.add(o);
        }
        orderService.saveBatch(orders);
        orders.forEach(item -> {
            BigDecimal amount = item.getAmount();
            BigDecimal amount1 = amount.divide(new BigDecimal("4"));
            BigDecimal amount2 = amount1.add(new BigDecimal("10"));
            BigDecimal amount3 = amount.subtract(amount1.add(amount2));
            List<BigDecimal> bigDecimals = Arrays.asList(amount1, amount2, amount3);

            for (int k=0; k<3; k++){
                OrderItem oi = new OrderItem();
                oi.setType(item.getType());
                oi.setOrderId(item.getId());
                oi.setName(item.getName() +"-" + item.getType());
                oi.setAmount(bigDecimals.get(k));
                orderItems.add(oi);
            }
        });
        orderItemService.saveBatch(orderItems);
    }
    @Test
    void orderInfos() {
        List<OrderInfo> orderInfos1 = orderMapper.findOrderInfos("1");
        List<OrderInfo> orderInfos2 = orderMapper.findOrderInfos("2");
        List<OrderInfo> orderInfos3 = orderMapper.findOrderInfos("3");
        System.err.println("type1-----------------------------");
        orderInfos1.forEach(System.err::println);

        System.err.println("type2-----------------------------");
        orderInfos2.forEach(System.out::println);
        System.err.println("type3-----------------------------");
        orderInfos3.forEach(System.err::println);
    }

    // 统计 order_item数量和金额 和 order金额
    @Test
    void statistic1() {
        List<OrderInfo1> orderInfos1 = orderMapper.statistic1("1");
        List<OrderInfo1> orderInfos2 = orderMapper.statistic1("2");
        List<OrderInfo1> orderInfos3 = orderMapper.statistic1("3");
        System.err.println("type1-----------------------------");
        orderInfos1.forEach(System.err::println);
        System.err.println("type2-----------------------------");
        orderInfos2.forEach(System.out::println);
        System.err.println("type3-----------------------------");
        orderInfos3.forEach(System.err::println);
    }

    @Test
    void page() {
//        List<OrderItem> orderItems1 = orderMapper.pageList("3", 1, 2);
//        System.err.println("type3-----------------------------");
//        System.out.println(orderItems1.size());
//        orderItems1.forEach(System.err::println);
//        System.err.println("type3--plus-----------------------------");

        IPage<OrderItem> page = orderItemService.lambdaQuery().eq(OrderItem::getType, "3").page(new Page<>(1, 2));
        page.getRecords().forEach(System.err::println);
    }

    @Test
    void orderItems() {
        List<OrderItem> orderItems1 = orderItemService.lambdaQuery().eq(OrderItem::getType, "1").list();
        List<OrderItem> orderItems2 = orderItemService.lambdaQuery().eq(OrderItem::getType, "2").list();
        List<OrderItem> orderItems3 = orderItemService.lambdaQuery().eq(OrderItem::getType, "3").list();
        System.err.println("type1-----------------------------");
        orderItems1.forEach(System.err::println);
        System.err.println("type2-----------------------------");
        orderItems2.forEach(System.out::println);
        System.err.println("type3-----------------------------");
        orderItems3.forEach(System.err::println);
    }

    @Test
    void contextLoads1() {
        List<Order> type1 = orderService.lambdaQuery().eq(Order::getType, "1").list();
        List<Order> type2 = orderService.lambdaQuery().eq(Order::getType, "2").list();
        List<Order> type3 = orderService.lambdaQuery().eq(Order::getType, "3").list();
        System.err.println("type1-----------------------------");
        type1.forEach(System.err::println);
        System.err.println("type2-----------------------------");
        type2.forEach(System.out::println);
        System.err.println("type3-----------------------------");
        type3.forEach(System.err::println);
    }

    @Test
    void update() {
        List<Order> type1 = orderService.lambdaQuery().eq(Order::getType, "1").list();
        type1.forEach(System.out::println);
        orderService.lambdaUpdate().set(Order::getName, "test-update").eq(Order::getType, "1").update();

        orderService.lambdaQuery()
                .list()
                .forEach(System.err::println);

    }

    @Test
    void getById(){

        Order o = orderService.getById("1377426875463344129");
        System.out.println(o);
    }

    @Test
    void removeAll() {
        orderService.remove(null);
        orderItemService.remove(null);
    }





}
